<?php
  // Here goes the page title:
  $pageTitle = 'AN Ressources web';
  // Here goes your feeds:
  $feed = "http://cultureweb.artsaucarre.be/ANRW/sharli/?do=rss";
  $sharliUrl = 'http://www.luuse.io/Shaarli/';
?>

<!DOCTYPE html>
<html>
  <head lang="fr-FR">
    <meta charset="UTF-8">
    <link rel="stylesheet" class="change" media="all" href="">
    <link rel="stylesheet" media="all" href="css/menu.css">
    <script type="text/javascript" src="js/lib/jquery-3.2.1.min.js"></script>

    <title><?= $pageTitle ?></title>
  </head>
  <body>

    <h1>Arts numériques ressources web</h1>

    <nav>
      <ul>      
        <li><h2>Changer le style →</h2></li>
        <?php
          $dir = 'css/';
          $files = scandir($dir);
          foreach ($files as $file) {
            if ($file == '.' OR $file == '..' OR $file == 'main.css' OR $file == 'fonts' or $file == 'menu.css') continue;
            $prenom = substr($file, 0, -4);
            echo '<li class="'.$prenom.'">'.$prenom.'</li>';
          }
        ?>
        <li>none</li>
      </ul>
    </nav>

    <div class="content">

      <?php

        $content = file_get_contents($feed);
        $xml = new SimpleXmlElement($content);
        ?>

        <?php
        foreach($xml->channel->item as $entry) {
          $categories = $entry->category;
          $dateParsed  = date_parse($entry->pubDate);
          ?>
          <article>
            <h2><?= $entry->title ?></h2>
            <h3><a href="<?= $entry->link ?>"><?= $entry->link ?></a></h3>
            <ul>
              <li>proposé par: <?= substr($categories[0], 1) ?></li>
              <li>année: <?= substr($categories[1], 1) ?></li>
              <li>catégorie: <?= substr($categories[2], 1) ?></li>
              <li>date: <?php echo $dateParsed['day'] . "/" . $dateParsed['month'] . "/" . $dateParsed['year']; ?></li>
            </ul>
            <p>
              <?= $entry->description ?>
            </p>
          </article>
        <?php } ?>
    </div>

    <footer>
      Generated with a <a href="<?= $sharliUrl ?>">Shaarli</a> feeds.<br/>
      Under <a href="https://www.gnu.org/licenses/gpl.html">GNU/GPLv3</a> License.<br/>
      <a href="https://gitlab.com/arts2artsnumeriques/ANRW"> Fork it</a>!
    </footer>

    <script type="text/javascript" src="js/functions.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
  </body>
</html>
