<?php /*
{
    "resource": {
        "data_dir": "data",
        "config": "data\/config.php",
        "datastore": "data\/datastore.php",
        "ban_file": "data\/ipbans.php",
        "updates": "data\/updates.txt",
        "log": "data\/log.txt",
        "update_check": "data\/lastupdatecheck.txt",
        "history": "data\/history.php",
        "raintpl_tpl": "tpl\/",
        "theme": "default",
        "raintpl_tmp": "tmp\/",
        "thumbnails_cache": "cache",
        "page_cache": "pagecache"
    },
    "security": {
        "ban_after": 4,
        "ban_duration": 1800,
        "session_protection_disabled": false,
        "open_shaarli": false,
        "allowed_protocols": [
            "ftp",
            "ftps",
            "magnet"
        ],
        "markdown_escape": true
    },
    "general": {
        "header_link": "?",
        "links_per_page": 20,
        "enabled_plugins": [
            "qrcode"
        ],
        "default_note_title": "Note: ",
        "timezone": "Europe\/Brussels",
        "title": "ANRW"
    },
    "updates": {
        "check_updates": true,
        "check_updates_branch": "stable",
        "check_updates_interval": 86400
    },
    "feed": {
        "rss_permalinks": true,
        "show_atom": true
    },
    "privacy": {
        "default_private_links": false,
        "hide_public_links": false,
        "force_login": false,
        "hide_timestamps": false,
        "remember_user_default": true
    },
    "thumbnail": {
        "enable_thumbnails": true,
        "enable_localcache": true
    },
    "redirector": {
        "url": "",
        "encode_url": true
    },
    "translation": {
        "language": "auto",
        "mode": "php",
        "extensions": []
    },
    "plugins": [],
    "credentials": {
        "login": "admin-sharli",
        "salt": "ee4427e769a39f731c650cc72a2c24578a50baad",
        "hash": "d4cf35ea18eae6c6fb008baccc4f9e97ee8b23e2"
    },
    "api": {
        "enabled": true,
        "secret": "1d298704f670"
    }
}
*/ ?>