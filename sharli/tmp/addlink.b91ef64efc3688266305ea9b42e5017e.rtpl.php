<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
<head>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("includes") . ( substr("includes",-1,1) != "/" ? "/" : "" ) . basename("includes") );?>

</head>
<body>
<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.header") . ( substr("page.header",-1,1) != "/" ? "/" : "" ) . basename("page.header") );?>

<div class="pure-g">
  <div class="pure-u-lg-1-3 pure-u-1-24"></div>
  <div id="addlink-form" class="page-form  page-form-light pure-u-lg-1-3 pure-u-22-24">
    <h2 class="window-title"><?php echo t( "Shaare a new link" );?></h2>
    <form method="GET" action="#" name="addform" class="addform">
      <div>
        <input type="text" name="post" placeholder="<?php echo t( 'URL or leave empty to post a note' );?>" class="autofocus">
      </div>
      <div>
        <input type="submit" value="<?php echo t( 'Add link' );?>">
      </div>
    </form>
  </div>
</div>
<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.footer") . ( substr("page.footer",-1,1) != "/" ? "/" : "" ) . basename("page.footer") );?>

</body>
</html>
