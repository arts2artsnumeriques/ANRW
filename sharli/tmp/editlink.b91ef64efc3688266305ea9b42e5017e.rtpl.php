<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
<head>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("includes") . ( substr("includes",-1,1) != "/" ? "/" : "" ) . basename("includes") );?>

</head>
<body>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.header") . ( substr("page.header",-1,1) != "/" ? "/" : "" ) . basename("page.header") );?>

  <div id="editlinkform" class="pure-g">
    <div class="pure-u-lg-1-5 pure-u-1-24"></div>
    <form method="post" name="linkform" class="page-form pure-u-lg-3-5 pure-u-22-24 page-form page-form-light">
      <h2 class="window-title">
        <?php if( !$link_is_new ){ ?><?php echo t( 'Edit' );?><?php } ?>

        <?php echo t( 'Shaare' );?>

      </h2>
      <input type="hidden" name="lf_linkdate" value="<?php echo $link["linkdate"];?>">
      <?php if( isset($link["id"]) ){ ?>

        <input type="hidden" name="lf_id" value="<?php echo $link["id"];?>">
      <?php } ?>

      <?php if( !$link_is_new ){ ?><div class="created-date"><?php echo t( 'Created:' );?> <?php echo format_date( $link["created"] );?></div><?php } ?>

      <div>
        <label for="lf_url"><?php echo t( 'URL' );?></label>
      </div>
      <div>
        <input type="text" name="lf_url" id="lf_url" value="<?php echo $link["url"];?>" class="lf_input autofocus">
      </div>
      <div>
      <label for="lf_title"><?php echo t( 'Title' );?></label>
      </div>
      <div>
        <input type="text" name="lf_title" id="lf_title" value="<?php echo $link["title"];?>" class="lf_input autofocus">
      </div>
      <div>
        <label for="lf_description"><?php echo t( 'Description' );?></label>
      </div>
      <div>
        <textarea name="lf_description" id="lf_description" class="autofocus"><?php echo $link["description"];?></textarea>
      </div>
      <div>
        <label for="lf_tags"><?php echo t( 'Tags' );?></label>
      </div>
      <div>
        <input type="text" name="lf_tags" id="lf_tags" value="<?php echo $link["tags"];?>" class="lf_input autofocus"
          data-list="<?php $counter1=-1; if( isset($tags) && is_array($tags) && sizeof($tags) ) foreach( $tags as $key1 => $value1 ){ $counter1++; ?><?php echo $key1;?>, <?php } ?>" data-multiple data-autofirst autocomplete="off" >
      </div>

      <div>
        <input type="checkbox"  name="lf_private" id="lf_private"
        <?php if( ($link_is_new && $default_private_links || $link["private"] == true) ){ ?>

          checked="checked"
        <?php } ?>>
        &nbsp;<label for="lf_private"><?php echo t( 'Private' );?></label>
      </div>

      <div id="editlink-plugins">
        <?php $counter1=-1; if( isset($edit_link_plugin) && is_array($edit_link_plugin) && sizeof($edit_link_plugin) ) foreach( $edit_link_plugin as $key1 => $value1 ){ $counter1++; ?>

          <?php echo $value1;?>

        <?php } ?>

      </div>


      <div class="submit-buttons center">
        <input type="submit" name="save_edit" class="" id="button-save-edit"
               value="<?php if( $link_is_new ){ ?><?php echo t( 'Save' );?><?php }else{ ?><?php echo t( 'Apply Changes' );?><?php } ?>">
        <?php if( !$link_is_new ){ ?>

        <a href="?delete_link&amp;lf_linkdate=<?php echo $link["id"];?>&amp;token=<?php echo $token;?>"
           title="" name="delete_link" class="button button-red confirm-delete">
          <?php echo t( 'Delete' );?>

        </a>
        <?php } ?>

      </div>

      <input type="hidden" name="token" value="<?php echo $token;?>">
      <?php if( $http_referer ){ ?>

        <input type="hidden" name="returnurl" value="<?php echo $http_referer;?>">
      <?php } ?>

    </form>
  </div>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.footer") . ( substr("page.footer",-1,1) != "/" ? "/" : "" ) . basename("page.footer") );?>

</body>
</html>
