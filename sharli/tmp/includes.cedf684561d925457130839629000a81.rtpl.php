<?php if(!class_exists('raintpl')){exit;}?><title><?php echo $pagetitle;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="alternate" type="application/atom+xml" href="<?php echo $feedurl;?>?do=atom<?php echo $searchcrits;?>" title="ATOM Feed" />
<link rel="alternate" type="application/rss+xml" href="<?php echo $feedurl;?>?do=rss<?php echo $searchcrits;?>" title="RSS Feed" />
<link href="tpl/default/./img/favicon.png" rel="shortcut icon" type="image/png" />
<link type="text/css" rel="stylesheet" href="tpl/default/./css/pure.min.css?v=<?php echo $version_hash;?>" />
<link type="text/css" rel="stylesheet" href="tpl/default/./css/grids-responsive.min.css?v=<?php echo $version_hash;?>">
<link type="text/css" rel="stylesheet" href="tpl/default/./css/pure-extras.css?v=<?php echo $version_hash;?>">
<link type="text/css" rel="stylesheet" href="tpl/default/./css/font-awesome.min.css?v=<?php echo $version_hash;?>" />
<link type="text/css" rel="stylesheet" href="inc/awesomplete.css?v=<?php echo $version_hash;?>" />
<link type="text/css" rel="stylesheet" href="tpl/default/./css/shaarli.css?v=<?php echo $version_hash;?>" />
<?php if( is_file('data/user.css') ){ ?>

  <link type="text/css" rel="stylesheet" href="data/user.css" />
<?php } ?>

<?php $counter1=-1; if( isset($plugins_includes["css_files"]) && is_array($plugins_includes["css_files"]) && sizeof($plugins_includes["css_files"]) ) foreach( $plugins_includes["css_files"] as $key1 => $value1 ){ $counter1++; ?>

  <link type="text/css" rel="stylesheet" href="<?php echo $value1;?>?v=<?php echo $version_hash;?>"/>
<?php } ?>

<link rel="search" type="application/opensearchdescription+xml" href="?do=opensearch" title="Shaarli search - <?php echo $shaarlititle;?>"/>