<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
<head>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("includes") . ( substr("includes",-1,1) != "/" ? "/" : "" ) . basename("includes") );?>

</head>
<body>
<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.header") . ( substr("page.header",-1,1) != "/" ? "/" : "" ) . basename("page.header") );?>


<div class="pure-g">
  <div class="pure-u-lg-1-6 pure-u-1-24"></div>
  <div class="pure-u-lg-2-3 pure-u-22-24 page-form page-visitor">
    <?php $countPics=$this->var['countPics']=count($linksToDisplay);?>

    <h2 class="window-title"><?php echo t( 'Picture Wall' );?> - <?php echo $countPics;?> <?php echo t( 'pics' );?></h2>

    <div id="plugin_zone_start_picwall" class="plugin_zone">
      <?php $counter1=-1; if( isset($plugin_start_zone) && is_array($plugin_start_zone) && sizeof($plugin_start_zone) ) foreach( $plugin_start_zone as $key1 => $value1 ){ $counter1++; ?>

        <?php echo $value1;?>

      <?php } ?>

    </div>

    <div id="picwall_container">
      <?php $counter1=-1; if( isset($linksToDisplay) && is_array($linksToDisplay) && sizeof($linksToDisplay) ) foreach( $linksToDisplay as $key1 => $value1 ){ $counter1++; ?>

        <div class="picwall_pictureframe">
          <?php echo $value1["thumbnail"];?><a href="<?php echo $value1["real_url"];?>"><span class="info"><?php echo $value1["title"];?></span></a>
          <?php $counter2=-1; if( isset($value1["picwall_plugin"]) && is_array($value1["picwall_plugin"]) && sizeof($value1["picwall_plugin"]) ) foreach( $value1["picwall_plugin"] as $key2 => $value2 ){ $counter2++; ?>

            <?php echo $value2;?>

          <?php } ?>

        </div>
      <?php } ?>

      <div class="clear"></div>
    </div>

    <div id="plugin_zone_end_picwall" class="plugin_zone">
      <?php $counter1=-1; if( isset($plugin_end_zone) && is_array($plugin_end_zone) && sizeof($plugin_end_zone) ) foreach( $plugin_end_zone as $key1 => $value1 ){ $counter1++; ?>

        <?php echo $value1;?>

      <?php } ?>

    </div>
  </div>
</div>

<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.footer") . ( substr("page.footer",-1,1) != "/" ? "/" : "" ) . basename("page.footer") );?>

<script src="inc/blazy-1.3.1.min.js"></script>
</body>
</html>

