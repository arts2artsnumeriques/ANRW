<?php if(!class_exists('raintpl')){exit;}?><!DOCTYPE html>
<html>
<head>
  <?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("includes") . ( substr("includes",-1,1) != "/" ? "/" : "" ) . basename("includes") );?>

</head>
<body>
<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.header") . ( substr("page.header",-1,1) != "/" ? "/" : "" ) . basename("page.header") );?>


<noscript>
  <div class="pure-g new-version-message pure-alert pure-alert-warning">
    <div class="pure-u-2-24"></div>
    <div class="pure-u-20-24">
      <?php echo t( 'You need to enable Javascript to change plugin loading order.' );?>

    </div>
  </div>
  <div class="clear"></div>
</noscript>

<form method="POST" action="?do=save_pluginadmin" name="pluginform" id="pluginform">
  <div class="pure-g">
    <div class="pure-u-lg-1-8 pure-u-1-24"></div>
    <div class="pure-u-lg-3-4 pure-u-22-24 page-form page-form-complete">
      <h2 class="window-title"><?php echo t( 'Plugin administration' );?></h2>

      <section id="enabled_plugins">
        <h3 class="window-subtitle"><?php echo t( 'Enabled Plugins' );?></h3>

        <div>
          <?php if( count($enabledPlugins)==0 ){ ?>

          <p><?php echo t( 'No plugin enabled.' );?></p>
          <?php }else{ ?>

          <table id="plugin_table">
            <thead>
              <tr>
                <th class="center"><?php echo t( 'Disable' );?></th>
                <th><?php echo t( 'Name' );?></th>
                <th><div class="pure-u-0 pure-u-lg-visible"><?php echo t( 'Description' );?></div></th>
                <th class="center"><?php echo t( 'Order' );?></th>
              </tr>
            </thead>
            <tbody>
              <?php $counter1=-1; if( isset($enabledPlugins) && is_array($enabledPlugins) && sizeof($enabledPlugins) ) foreach( $enabledPlugins as $key1 => $value1 ){ $counter1++; ?>

                <tr data-line="<?php echo $key1;?>" data-order="<?php echo $counter1;?>" class="main-row">
                  <td class="center"><input type="checkbox" name="<?php echo $key1;?>" id="<?php echo $key1;?>" checked="checked"></td>
                  <td class="center">
                    <label for="<?php echo $key1;?>"><strong><?php echo str_replace('_', ' ', $key1); ?></strong></label>
                  </td>
                  <td><div class="pure-u-0 pure-u-lg-visible"><label for="<?php echo $key1;?>"><?php echo $value1["description"];?></label></div></td>
                  <td class="center">
                    <?php if( count($enabledPlugins)>1 ){ ?>

                    <a href="#" class="order order-up">▲</a>
                    <a href="#" class="order order-down">▼</a>
                    <?php } ?>

                    <input type="hidden" name="order_<?php echo $key1;?>" value="<?php echo $counter1;?>">
                  </td>
                </tr>
                <tr data-line="<?php echo $key1;?>" data-order="<?php echo $counter1;?>" class="pure-u-lg-0 mobile-row">
                  <td colspan="4"><label for="<?php echo $key1;?>"><?php echo $value1["description"];?></label></td>
                </tr>
              <?php } ?>

            </tbody>
            <tfoot>
              <tr>
                <th class="center"><?php echo t( 'Disable' );?></th>
                <th><?php echo t( 'Name' );?></th>
                <th><div class="pure-u-0 pure-u-lg-visible"><?php echo t( 'Description' );?></div></th>
                <th class="center"><?php echo t( 'Order' );?></th>
              </tr>
            </tfoot>
          </table>
          <?php } ?>

        </div>
      </section>

      <section id="disabled_plugins">
        <h3 class="window-subtitle"><?php echo t( 'Disabled Plugins' );?></h3>

        <div>
          <?php if( count($disabledPlugins)==0 ){ ?>

          <p><?php echo t( 'No plugin disabled.' );?></p>
          <?php }else{ ?>

          <table>
            <thead>
              <tr>
                <th class="center"><?php echo t( 'Enable' );?></th>
                <th><?php echo t( 'Name' );?></th>
                <th><div class="pure-u-0 pure-u-lg-visible"><?php echo t( 'Description' );?></div></th>
              </tr>
            </thead>
            <tbody>
              <?php $counter1=-1; if( isset($disabledPlugins) && is_array($disabledPlugins) && sizeof($disabledPlugins) ) foreach( $disabledPlugins as $key1 => $value1 ){ $counter1++; ?>

                <tr class="main-row">
                  <td class="center"><input type="checkbox" name="<?php echo $key1;?>" id="<?php echo $key1;?>"></td>
                  <td class="center">
                    <label for="<?php echo $key1;?>"><strong><?php echo str_replace('_', ' ', $key1); ?></strong></label>
                  </td>
                  <td><div class="pure-u-0 pure-u-lg-visible">
                    <label for="<?php echo $key1;?>"><?php echo $value1["description"];?></label>
                  </div></td>
                </tr>
                <tr class="pure-u-lg-0 mobile-row">
                  <td colspan="3"><label for="<?php echo $key1;?>"><?php echo $value1["description"];?></label></td>
                </tr>
              <?php } ?>

            </tbody>
            <tfoot>
              <tr>
                <th class="center"><?php echo t( 'Enable' );?></th>
                <th><?php echo t( 'Name' );?></th>
                <th><div class="pure-u-0 pure-u-lg-visible"><?php echo t( 'Description' );?></div></th>
              </tr>
            </tfoot>
          </table>
          <?php } ?>

        </div>
      </section>

      <div class="center more">
        <?php echo t( "More plugins available" );?>

        <a href="doc/Community-&-Related-software.html#third-party-plugins"><?php echo t( "in the documentation" );?></a>.
      </div>
      <div class="center">
        <input type="submit" value="<?php echo t( 'Save' );?>" name="save">
      </div>
    </div>
  </div>
  <input type="hidden" name="token" value="<?php echo $token;?>">
</form>

<form action="?do=save_pluginadmin" method="POST">
  <div class="pure-g">
    <div class="pure-u-lg-1-8 pure-u-1-24"></div>
    <div class="pure-u-lg-3-4 pure-u-22-24 page-form page-form-light">
      <h2 class="window-title"><?php echo t( 'Plugin configuration' );?></h2>
      <section id="plugin_parameters">
        <div>
          <?php if( count($enabledPlugins)==0 ){ ?>

            <p><?php echo t( 'No plugin enabled.' );?></p>
          <?php }else{ ?>

            <?php $counter1=-1; if( isset($enabledPlugins) && is_array($enabledPlugins) && sizeof($enabledPlugins) ) foreach( $enabledPlugins as $key1 => $value1 ){ $counter1++; ?>

              <?php if( count($value1["parameters"]) > 0 ){ ?>

                <div class="plugin_parameters">
                  <h3 class="window-subtitle"><?php echo str_replace('_', ' ', $key1); ?></h3>
                  <?php $counter2=-1; if( isset($value1["parameters"]) && is_array($value1["parameters"]) && sizeof($value1["parameters"]) ) foreach( $value1["parameters"] as $key2 => $value2 ){ $counter2++; ?>

                  <div class="plugin_parameter">
                    <p class="float_label">
                      <label for="<?php echo $key2;?>">
                        <code><?php echo $key2;?></code>
                        <?php if( isset($value2["desc"]) ){ ?>

                          &middot; <?php echo $value2["desc"];?>

                        <?php } ?>

                      </label>
                    </p>
                    <div class="float_input">
                      <input name="<?php echo $key2;?>" value="<?php echo $value2["value"];?>" id="<?php echo $key2;?>" type="text" />
                    </div>
                  </div>
                  <?php } ?>

                </div>
              <?php } ?>

            <?php } ?>

          <?php } ?>

          <div class="center">
            <input type="submit" name="parameters_form" value="<?php echo t( 'Save' );?>"/>
          </div>
        </div>
      </section>
    </div>
  </div>
</form>

<?php $tpl = new RainTpl;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("page.footer") . ( substr("page.footer",-1,1) != "/" ? "/" : "" ) . basename("page.footer") );?>

<script src="inc/plugin_admin.js"></script>

</body>
</html>
