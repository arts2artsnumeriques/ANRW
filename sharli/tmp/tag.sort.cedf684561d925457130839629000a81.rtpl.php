<?php if(!class_exists('raintpl')){exit;}?><div class="pure-g">
  <div class="pure-u-1 pure-alert pure-alert-success tag-sort">
    <?php echo t( 'Sort by:' );?>

    <a href="?do=tagcloud" title="cloud"><?php echo t( 'Cloud' );?></a> &middot;
    <a href="?do=taglist&sort=usage" title="cloud"><?php echo t( 'Most used' );?></a> &middot;
    <a href="?do=taglist&sort=alpha" title="cloud"><?php echo t( 'Alphabetical' );?></a>
  </div>
</div>